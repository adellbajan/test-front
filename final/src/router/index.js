import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Index",
    component: require("../components/pages/Index").default
  },
  {
    path: "/myorder",
    name: "MyOrder",
    component: require("../components/pages/MyOrder").default
  },
  {
    path: "/customersorder",
    name: "CustomersOrder",
    component: require("../components/pages/CustomersOrder").default
  }
];

const router = new VueRouter({
  mode: "history",
  routes,
});

export default router;

import Vue from 'vue'
import App from './App.vue'
import router from './router'

window.Popper = require('popper.js').default;
window.$ = window.jQuery = require('jquery');
require('bootstrap');
import 'bootstrap/dist/css/bootstrap.min.css'

import "@/assets/css/fontawesome.css"
import "@/assets/css/style.css"



Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App),

}).$mount('#app')
